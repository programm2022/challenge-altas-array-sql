(function () {
    if (!window.openDatabase) {
        alert("Este navegador no soporta la API WebSQL");
    } else {
        console.log("Este navegador soporta la API WebSQL");
    }
})();

let dbComercio;

function openDB() {
    dbComercio = openDatabase("dbComercio", "1.0", "Base de datos del comercio", 2 * 1024);
    if (!dbComercio) {
        alert("No se pudo crear la base de datos");
        javascript_abort();
    } else {
        console.log("La base de datos se creo correctamente");
    }
    function javascript_abort() {
        throw new error("Por no poder abrir la base de datos, abortamos javascript");
    }
    console.log(dbComercio.version);
}

function createtableArticulos() {
    console.log("Creando la tabla articulos");
    dbComercio.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS Articulos (id integer primary key autoincrement, descripcion TEX, imagen TEX)")
    });
}

let producto = {
    id: 0,
    descripcion: "",
    imagen: ""
}
function agregar() {
    createtableArticulos();
    console.log("Agregamos Artículos");
    dbComercio.transaction(function (tx) {
        // ID NO SE CARGA PORQUE LO DEFINIMOS AUTOINCREMENT
        producto.descripcion = document.querySelector("#desc").value;
        producto.imagen = document.querySelector("#dmgI").value;
        tx.executeSql(
            "INSERT INTO Articulos(descripcion, imagen) VALUES(?,?)", [producto.descripcion, producto.imagen], itemInserted
        );
    });

}
function itemInserted(tx, results) {
    console.log("id: ", results.insertId);
}
let cantidad;

function mostrarArticulos() {
    console.log("Mostramos artículos");
    dbComercio.transaction(function (tx) {
        tx.executeSql("SELECT * from Articulos ", [], function (tx, results) {
            cantidad = results.rows.length;
            armarTemplate(results);
        });
    });
}
function armarTemplate(results) {
    let template = "", row;
    for (let i = 0; i < cantidad; i++) {
        row = results.rows.item(i);
        producto.id = row.id;
        producto.descripcion = row.descripcion;
        producto.imagen = row.imagen;

        template += `<article>
        <h3 class="descripcion">${producto.descripcion}</h3>
        <img src="${producto.imagen}" class="imagen">
                     </article>`
    }
    document.querySelector("#productos").innerHTML = template;
}
function dropTable() {
    dbComercio.transaction(function (tx) {
        tx.executeSql(
            "DROP TABLE Articulos", [],
            function (tx, results) {
                console.log("Tabla eliminada");
                document.querySelector("#productos").innerHTML = "";
            },
            function (tx, error) {
                console.error("Error: ", error)
            }
        );
    });

}
function listado() {
    let arrayMisproductos =[];
    dbComercio.transaction(function (tx){
        tx.executeSql("SELECT * from Articulos ", [], function (tx, results){
            cantidad = results.rows.length;
            armarTemplate(results);
            for (let i = 0; i < cantidad; i++) {
                row = results.rows.item(i);
                producto = row;
                arrayMisproductos[i]= row;
            }
            localStorage.setItem('articulos',JSON.stringify(arrayMisproductos));
            location.href = 'resultado.html';
        });
    });
}

openDB();
createtableArticulos();
mostrarArticulos();